//
//  Transicao.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Transicao.h"

Transicao::Transicao()
{
    this->simbolo = 0;
    this->prox = 0;
}

Transicao::Transicao(char simbolo, Estado *prox)
{
    this->simbolo = simbolo;
    this->prox = prox;
}

Transicao::~Transicao()
{
    if (prox)
        delete prox;    
}

void Transicao::setSimbolo(char simbolo)
{
    this->simbolo = simbolo;
}

void Transicao::setProximoEstado(Estado *prox)
{
    this->prox = prox;
}

char Transicao::getSimbolo()
{
    return this->simbolo;
}

Estado* Transicao::getProximoEstado()
{
    return this->prox;
}

bool Transicao::operator==(Transicao transicao)
{
    return (this->simbolo == transicao.getSimbolo());
}

