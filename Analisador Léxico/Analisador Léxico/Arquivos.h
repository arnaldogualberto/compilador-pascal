/* 
 * File:   Arquivos.h
 * Author: ygd
 *
 * Created on 16 de Janeiro de 2013, 20:48
 */

#ifndef ARQUIVOS_H
#define	ARQUIVOS_H

using namespace std;

class Arquivos {
public:
    Arquivos();
    Arquivos(const Arquivos& orig);
    virtual ~Arquivos();
    FILE* abrirArquivo(const char* caminho);
    void fecharArquivo(FILE *f );
private:

};

#endif	/* ARQUIVOS_H */

