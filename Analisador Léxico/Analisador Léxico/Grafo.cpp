//
//  Grafo.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 16/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Grafo.h"

Grafo::Grafo()
{
    this->e1 = new Estado("1", Estado_Inicial);
    this->e2 = new Estado("38", Estado_Final, Identificador);
    this->e3 = new Estado("39", Estado_Final, Inteiro);
    this->e4 = new Estado("73", Estado_Final, Real);
    this->e5 = new Estado("74", Estado_Final, Real);
    this->e6 = new Estado("28", Estado_Final, Delimitador);
    this->e7 = new Estado("29", Estado_Final, Delimitador);
    this->e8 = new Estado("30", Estado_Final, Operador_Aditivo);
    this->e9 = new Estado("31", Estado_Final, Delimitador);
    this->e10 = new Estado("32", Estado_Final, Operador_Multiplicativo);
    this->e11 = new Estado("33", Estado_Final, Delimitador);
    this->e12 = new Estado("35", Estado_Final, Delimitador);
    this->e13 = new Estado("36", Estado_Final, Delimitador);
    this->e14 = new Estado("75", Estado_Final, Operador_Aditivo);
    this->e15 = new Estado("76", Estado_Final, Operador_Multiplicativo);
    this->e16 = new Estado("80", Estado_Final, Operador_Relacional);
    this->e17 = new Estado("81", Estado_Final, Operador_Relacional);
    this->e18 = new Estado("82", Estado_Final, Operador_Relacional);
    this->e19 = new Estado("34", Estado_Final, Atribuicao);
    this->e20 = new Estado("37", Estado_Transitorio);
    this->e21 = new Estado("66", Estado_Final); //caracteres especiais
    this->e22 = new Estado("2", Estado_Final, Palavra_Reservada);
    
    iniciaEstado1();
    iniciaEstado2();
    iniciaEstado3();
    iniciaEstado4();
    iniciaEstado5();
    iniciaEstado6();
    iniciaEstado7();
    iniciaEstado8();
    iniciaEstado9();
    iniciaEstado10();
    iniciaEstado11();
    iniciaEstado12();
    iniciaEstado13();
    iniciaEstado14();
    iniciaEstado15();
    iniciaEstado16();
    iniciaEstado17();
    iniciaEstado18();
    iniciaEstado19();
    iniciaEstado20();
    iniciaEstado21();
    iniciaEstado22();
}

Grafo::~Grafo()
{
    
}

LinkedList<Estado>* Grafo::estados()
{
    LinkedList<Estado> *estados = new LinkedList<Estado>();
    estados->addLast(this->e1);
    estados->addLast(this->e2);
    estados->addLast(this->e3);
    estados->addLast(this->e4);
    estados->addLast(this->e5);
    estados->addLast(this->e6);
    estados->addLast(this->e7);
    estados->addLast(this->e8);
    estados->addLast(this->e9);
    estados->addLast(this->e10);
    estados->addLast(this->e11);
    estados->addLast(this->e12);
    estados->addLast(this->e13);
    estados->addLast(this->e14);
    estados->addLast(this->e15);
    estados->addLast(this->e16);
    estados->addLast(this->e17);
    estados->addLast(this->e18);
    estados->addLast(this->e19);
    estados->addLast(this->e20);
    estados->addLast(this->e21);
    
    return estados;
}

void Grafo::Grafo::iniciaEstado1()
{
    //A..Z
    for (char i = 65; i <= 90 ; ++i) {
        this->e1->addTransicao(new Transicao(i, this->e2));
    }
    
    //a..z
    for (char i = 97; i <= 122 ; ++i) {
        this->e1->addTransicao(new Transicao(i, this->e2));
    }
    
    //0..9
    for (char i = 48; i <= 57; ++i) {
        this->e1->addTransicao(new Transicao(i, this->e3));
    }
    
    this->e1->addTransicao(new Transicao(',', this->e6));
    this->e1->addTransicao(new Transicao('.', this->e7));
    this->e1->addTransicao(new Transicao('+', this->e8)); 
    this->e1->addTransicao(new Transicao(';', this->e9));
    this->e1->addTransicao(new Transicao('*', this->e10));
    this->e1->addTransicao(new Transicao('(', this->e11));
    this->e1->addTransicao(new Transicao(':', this->e12));
    this->e1->addTransicao(new Transicao(')', this->e13));
    this->e1->addTransicao(new Transicao('-', this->e14));
    this->e1->addTransicao(new Transicao('/', this->e15));
    this->e1->addTransicao(new Transicao('<', this->e16));
    this->e1->addTransicao(new Transicao('>', this->e18));
    this->e1->addTransicao(new Transicao('=', this->e19));
    this->e1->addTransicao(new Transicao('{', this->e20));
    this->e1->addTransicao(new Transicao('\t', this->e1));
    this->e1->addTransicao(new Transicao(' ', this->e1));
    
    this->e1->addTransicao(new Transicao('~', this->e21));
    this->e1->addTransicao(new Transicao(127, this->e21));
    this->e1->addTransicao(new Transicao('^', this->e21));
    this->e1->addTransicao(new Transicao('\'', this->e21));
    this->e1->addTransicao(new Transicao('$', this->e21));
    this->e1->addTransicao(new Transicao('&', this->e21));
}

void Grafo::iniciaEstado2()
{
    //A..Z
    for (char i = 65; i <= 90 ; ++i) {
        this->e2->addTransicao(new Transicao(i, this->e2));
    }
    
    //a..z
    for (char i = 97; i <= 122 ; ++i) {
        this->e2->addTransicao(new Transicao(i, this->e2));
    }
    
    //0..9
    for (char i = 48; i <= 57; ++i) {
        this->e2->addTransicao(new Transicao(i, this->e2));
    }
    
    this->e2->addTransicao(new Transicao('_', e2));
}

void Grafo::iniciaEstado3()
{
    //0..9
    for (char i = 48; i <= 57; ++i) {
        this->e3->addTransicao(new Transicao(i, this->e3));
    }
    this->e3->addTransicao(new Transicao('.', this->e4));
}

void Grafo::iniciaEstado4()
{
    //0..9
    for (char i = 48; i <= 57; ++i) {
        this->e4->addTransicao(new Transicao(i, this->e5));
    }
}

void Grafo::iniciaEstado5()
{
    //0..9
    for (char i = 48; i <= 57; ++i) {
        this->e5->addTransicao(new Transicao(i, this->e5));
    }
}

void Grafo::iniciaEstado6()
{
    
}

void Grafo::iniciaEstado7()
{
    
}

void Grafo::iniciaEstado8()
{
    
}

void Grafo::iniciaEstado9()
{
    
}

void Grafo::iniciaEstado10()
{
    
}

void Grafo::iniciaEstado11()
{
    
}

void Grafo::iniciaEstado12()
{
    this->e12->addTransicao(new Transicao('=', this->e19));
}

void Grafo::iniciaEstado13()
{
    
}

void Grafo::iniciaEstado14()
{
    
}

void Grafo::iniciaEstado15()
{
    
}

void Grafo::iniciaEstado16()
{
    this->e16->addTransicao(new Transicao('=', this->e19));
    this->e16->addTransicao(new Transicao('>', this->e17));
}

void Grafo::iniciaEstado17()
{
    
}

void Grafo::iniciaEstado18()
{
    this->e18->addTransicao(new Transicao('=', this->e19));
}


void Grafo::iniciaEstado19()
{
    
}

void Grafo::iniciaEstado20()
{
    //A..Z
    for (char i = 0; i <= 126 ; ++i) {
        if(i!='}')
            this->e20->addTransicao(new Transicao(i, this->e20));
    }
    
    this->e20->addTransicao(new Transicao('}', this->e1));
    this->e20->addTransicao(new Transicao('\"', this->e20));
}

void Grafo::iniciaEstado21()
{
    
}

void Grafo::iniciaEstado22()
{
    
}

Estado* Grafo::getEstado22()
{
    return this->e22;
}