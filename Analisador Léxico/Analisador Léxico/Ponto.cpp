//
//  Ponto.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Ponto.h"

//Construtores
Ponto::Ponto()
{
    this->x = 0;
    this->y = 0;
}

Ponto::Ponto(int *x, int *y)
{
    this->x = x;
    this->y = y;
}

//Destrutor
Ponto::~Ponto()
{
    if(x)
    {
        delete x;
    }
    
    if (y)
    {
        delete y;
    }
    
    //delete[] libera um array
}

//Acessors
void Ponto::setX(int *x)
{
    this->x = x;
}

void Ponto::setY(int *y)
{
    this->y = y;
}

int* Ponto::getX()
{
    return this->x;
}

int* Ponto::getY()
{
    return this->y;
}