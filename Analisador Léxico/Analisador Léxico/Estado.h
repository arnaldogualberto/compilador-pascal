//
//  Estado.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Estado__
#define __Analisador_Le_xico__Estado__

#import "LinkedList.h"
#import "Transicao.h"

#include <string.h>

typedef enum{
    Estado_Inicial,
    Estado_Transitorio,
    Estado_Final
} Tipo;

typedef enum{
    Atribuicao,
    Delimitador,
    Identificador,
    Inteiro,
    Real,
    Operador_Aditivo,
    Operador_Multiplicativo,
    Operador_Relacional,
    Operador_Diferenca,
    Palavra_Reservada
} Classificacao;

//Importante para o compilador por causa do import
class Transicao;

class Estado {
    LinkedList<Transicao> *transicoes;
    string nome;
    Tipo tipo;
    Classificacao classificacao;

public:
    Estado();
    Estado(string nome, Tipo t);
    Estado(string nome, Tipo t, Classificacao c);
    
    virtual ~Estado();

    void addTransicao(Transicao *t);
    Estado* proximoEstado(char simbolo);
    
    void listarTransicoes();

    void setNome(string nome);
    void setTransicoes(LinkedList<Transicao> *transicoes);
    void setTipo(Tipo t);
    void setClassificao(Classificacao c);
    
    LinkedList<Transicao>* getTransicoes();
    string getNome();
    string getTipo();
    string getClassificacao();
};

#endif /* defined(__Analisador_Le_xico__Estado__) */
