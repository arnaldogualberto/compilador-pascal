//
//  Pilha.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Pilha__
#define __Analisador_Le_xico__Pilha__

#include <iostream>
#include "LinkedList.h"
#include "Variavel.h"

class Pilha : public LinkedList<Variavel> {
    
public:
    Pilha();
    
    bool temSimbolo(string simbolo);
    bool estaNoEscopo(string simbolo);
    void desempilhaEscopo();
    string getTipoDaVariavel(string variavel);
    
    void setTipoDasVariaveisNaoMarcadas(string nomeTipo);

    void imprimePilha();
};

#endif /* defined(__Analisador_Le_xico__Pilha__) */
