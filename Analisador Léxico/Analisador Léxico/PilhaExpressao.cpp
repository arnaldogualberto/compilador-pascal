//
//  PilhaExpressao.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 09/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "PilhaExpressao.h"

PilhaExpressao::PilhaExpressao()
{
    
}

string PilhaExpressao::tipoBase()
{
    return getLast()->getTipoVariavel();
}

bool PilhaExpressao::atualizaTopo()
{
    if(getSize() <= 1) return true;

    string topo = getFirst()->getTipoVariavel();
    removeFirst();
    string subtopo = getFirst()->getTipoVariavel();
    
    string tipo_resultante = tipoResultante(topo, subtopo);
//    cout << "Tipo Resultante: " << tipo_resultante << endl;
    
    if(tipo_resultante.compare("erro") == 0) return false;
    
//    cout << "Tipo Base: " << tipoBase() << endl;
    
    if(tipo_resultante.compare("real") == 0 && tipoBase().compare("inteiro") == 0){
     return false;
    }
    else if(tipo_resultante.compare("boolean") == 0 && (tipoBase().compare("inteiro") == 0 || tipoBase().compare("real") == 0)){
        return false;
    }
    else if(tipoBase().compare("boolean") ==0 && (tipo_resultante.compare("inteiro") == 0 || tipo_resultante.compare("real") == 0)){
        return false;
    }
    
    removeFirst();
    addFirst(new Expressao(tipo_resultante));
    return true;
}

string PilhaExpressao::tipoResultante(string topo, string subtopo)
{
    if( topo.compare("inteiro") == 0 && subtopo.compare("inteiro") == 0 ) return "inteiro";
    else if( topo.compare("inteiro") == 0 && subtopo.compare("real") == 0 ) return "real";
    else if( topo.compare("real") == 0 && subtopo.compare("inteiro") == 0 ) return "real";
    else if( topo.compare("real") == 0 && subtopo.compare("real") == 0 ) return "real";
    else if( topo.compare("boolean") == 0 && subtopo.compare("boolean") == 0 ) return "boolean";
    else return "erro";
}

void PilhaExpressao::esvaziaPilha()
{
    clear();
}

void PilhaExpressao::imprimePilha()
{
    Expressao *atual = getFirst();
    
    while (atual != 0) {
        cout << atual->getTipoVariavel() << endl;
        atual = getNext();
    }
}
