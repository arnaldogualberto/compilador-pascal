//
//  Variavel.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 08/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Variavel.h"

Variavel::Variavel()
{
    
}

Variavel::Variavel(string identificador, Tipo_Variavel t)
{
    this->identificador = identificador;
    this->tipo = t;
    
}

void Variavel::setIdentificador(string identificador)
{
    this->identificador = identificador;
}

void Variavel::setTipo(Tipo_Variavel t)
{
    this->tipo = t;
}

string Variavel::getIdentificador()
{
    return this->identificador;
}

string Variavel::getTipo()
{
    switch (this->tipo) {
        case 0:
            return "não marcado";
            break;
            
        case 1:
            return "mark";
            break;

        case 2:
            return "inteiro";
            break;
            
        case 3:
            return "real";
            break;
            
        case 4:
            return "boolean";
            break;

        case 5:
            return "não é variável";
            break;
            
        default: 
            break;
    }
}
