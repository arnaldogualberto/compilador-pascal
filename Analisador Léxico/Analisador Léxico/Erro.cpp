//
//  Erro.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 14/02/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Erro.h"

Erro::Erro()
{
    
}

Erro::Erro(string tipo, int linha, string token)
{
    this->tipo = tipo;
    this->linha = linha;
    this->token = token;
}

Erro::Erro(string tipo)
{
    this->tipo = tipo;
}

void Erro::setTipo(string tipo)
{
    this->tipo = tipo;
}

void Erro::setLinha(int linha)
{
    this->linha = linha;
}

void Erro::setToken(string token)
{
    this->token = token;
}

string Erro::getTipo()
{
    return this->tipo;
}

int Erro::getLinha()
{
    return this->linha;
}

string Erro::getToken()
{
    return this->token;
}

void Erro::mostraErro()
{
    if(this->token != ""){
        cout << "Fatal: Erro de Sintaxe, esperado " << this->tipo << ", mas '" << this->token << "' encontrado. Linha: " << this->linha << endl;
        cout << "Compilação Abortada!" << endl;
    }else if(this->tipo.compare("tipos incompativeis") == 0){
        mostraErroSemantico();
    }else{
        cout << "Fatal: Erro de Sintaxe, " << tipo << " esperado." <<endl;
        cout << "Compilação Abortada!" << endl;        
    }
}

void Erro::mostraErroSemantico(){
    cout << "Fatal: Erro Semântico, " << this->tipo << ". Linha: " << this->linha << endl;
    cout << "Compilação Abortada!" << endl;
}