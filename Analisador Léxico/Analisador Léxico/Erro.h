//
//  Erro.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 14/02/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Erro__
#define __Analisador_Le_xico__Erro__

#include <iostream>
//#include <cstdlib>
#include <string>

using namespace std;

class Erro {
    string tipo;
    int linha;
    string token;
    
public:
    Erro();
    Erro(string tipo, int linha, string token);
    Erro(string tipo);
    
    void setTipo(string tipo);
    void setLinha(int linha);
    void setToken(string token);

    string getTipo();
    int getLinha();
    string getToken();
    
    void mostraErro();
    void mostraErroSemantico();
};

#endif /* defined(__Analisador_Le_xico__Erro__) */
