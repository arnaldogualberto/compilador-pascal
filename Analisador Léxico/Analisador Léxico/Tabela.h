//
//  Tabela.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 16/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Tabela__
#define __Analisador_Le_xico__Tabela__

#include <iostream>
#include <string.h>

#include "Estado.h"
#include "Grafo.h"

#define MAIOR_CLASSIFICADOR 23 //Operador Multiplicativo

class Tabela {
    string token;
    string classificacao;
    Estado *estado;
    int linha;
    
public:
    Tabela();
    Tabela(string token, string classificacao, int linha);
    Tabela(string token, Estado *estado, int linha);
    void mostrarTabela(LinkedList<Tabela> *tab);
    
    string getToken();
    string getClassificacao();
    Estado* getEstado();
    int getLinha();
    
private:
    int tamanhoStringClassificacao(string classificao);
    void modificaIdentificador(Tabela *t);
};

#endif /* defined(__Analisador_Le_xico__Tabela__) */
