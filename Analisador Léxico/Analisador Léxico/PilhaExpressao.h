//
//  PilhaExpressao.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 09/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__PilhaExpressao__
#define __Analisador_Le_xico__PilhaExpressao__

#include <iostream>

#include "LinkedList.h"
#include "Expressão.h"

class PilhaExpressao: public LinkedList<Expressao> {
    
public:
    PilhaExpressao();

    string tipoBase();
    bool atualizaTopo();
    void esvaziaPilha();

    void imprimePilha();

private:
    string tipoResultante(string topo, string subtopo);

};

#endif /* defined(__Analisador_Le_xico__PilhaExpressao__) */
