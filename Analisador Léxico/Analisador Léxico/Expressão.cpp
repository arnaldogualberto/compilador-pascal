//
//  Expressão.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 09/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Expressão.h"

Expressao::Expressao()
{
    
}

Expressao::Expressao(string tipo_variavel)
{
    this->tipo_variavel = tipo_variavel;
}

void Expressao::setTipoVariavel(string tipo_variavel)
{
    this->tipo_variavel = tipo_variavel;
}

string Expressao::getTipoVariavel()
{
    return this->tipo_variavel;
}