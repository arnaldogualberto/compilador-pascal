//
//  main.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include <iostream>
#include "Estado.h"
#include "Transicao.h"
#include "Grafo.h"
#include "LinkedList.h"
#include "Arquivos.h"
#include "Tabela.h"
#include "AnalisadorSintatico.h"
//#include "File.h"
#include "Pilha.h"

//Declaração de Bloco.
using namespace std;

int main(int argc, const char * argv[])
{
//    Pilha *pilha = new Pilha();
//    pilha->addLast("a");
//    pilha->addLast("b");
//    pilha->addLast("c");
//    
//    string *atual = pilha->getFirst();
//    while (atual != 0) {
//        cout << *(atual) << endl;
//        atual = pilha->getNext();
//    }
    
    Grafo *grafo = new Grafo();
    LinkedList<Estado> *todos_estados = new LinkedList<Estado>();
    todos_estados = grafo->estados();
    
    LinkedList<Tabela> *tabela = new LinkedList<Tabela>();
    
    Arquivos *arq = new Arquivos();
    FILE *f = arq->abrirArquivo("/Users/Arnaldo/UFPB/7º Período/Compiladores/TesteSintatico.pas");
//    FILE *f = arq->abrirArquivo("/Users/Arnaldo/UFPB/7º Período/Compiladores/testeLexico");
    
    if (!f){
        cout << "Erro ao abrir o arquivo";
        return 0;
    }
    
    Estado *ant = todos_estados->getFirst();
    Estado *inicial = todos_estados->getFirst();
    Estado *atual ;
    char c = getc(f);
    string token = "";
    int linha = 1;
    
    while(!feof(f)){
        //Erro: Comentário dentro de comentário
        if(c == '{' && ant->getNome() == "37")
            break;
        
        //Erro: Símbolo não pertencente à linguagem
        if(ant->getNome()=="66")
            break;
        
        if(c=='\n'){
            if(token!="" && ant->getNome() != "37"){
                tabela->addLast(new Tabela(token, ant, linha));
                ant = inicial;
            }
            token="";
            linha++;
            c=getc(f);
        }
        
        atual = ant->proximoEstado(c);
        if(!atual){
            if(token!=""){
//                string classificacao = new string("testes");
                tabela->addLast(new Tabela(token, ant, linha));
                token = "";
            }
            ant = inicial;
        }else{
            ant = atual;
            
            if(c!=' ' && c!= '\t' && c != '\0') token += c;
            if (c == '}') token = "";
            
            c = getc(f);
        }
    }
    
    if(ant->getNome() == "37"){
        cout << "\nErro: Comentário não fechado!";
        return 0;
    }else if(ant->getNome()=="66"){
        cout << "\nErro: Caractere especial invalido na linha: " << linha << "\n";
        return 0;
    }
    else if(token != "")
        tabela->addLast(new Tabela(token, ant, linha));
    
    Tabela *t = new Tabela();
    t->mostrarTabela(tabela);
    
    cout << endl;
    
    AnalisadorSintatico *analisadorSintatico = new AnalisadorSintatico(tabela);
    analisadorSintatico->initAnaliseSintatica();
    analisadorSintatico->imprimeErros();
    
    if(analisadorSintatico->getNumeroDeErros() == 0)
        cout << "Programa compilado com Sucesso!" << endl;
    
    arq->fecharArquivo(f);
}
