//
//  AnalisadorSintatico.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 14/02/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__AnalisadorSintatico__
#define __Analisador_Le_xico__AnalisadorSintatico__

#include <iostream>
#import "LinkedList.h"
#import "Tabela.h"
#import "Erro.h"
#import "Pilha.h"
#import "PilhaExpressao.h"

/*
 Legenda:
    Aux: Sem Recursão à Esquerda
 */

class AnalisadorSintatico {
    LinkedList<Tabela> *saidaLexico;
    LinkedList<Erro> *erros;
    int contadorComandoComposto;
    Pilha *pilha;
    PilhaExpressao *pilhaExpressao;

public:
    AnalisadorSintatico();
    AnalisadorSintatico(LinkedList<Tabela> *saidaLexico);
    void imprimeErros();
    void initAnaliseSintatica();
    
    int getNumeroDeErros();
    
private:
    Tabela* proximoSimbolo();
    void declaracoesDeVariaveis();
    void listaDeDeclaracoesDeVariaveis();
    void listaDeDeclaracoesDeVariaveisAux();
    void listaDeIdentificadores();
    void listaDeIdentificadoresAux();
    void tipo();
    void declaracoesDeSubprogramas();
    void declaracoesDeSubprogramasAux();
    void declaracaoDeSubprograma();
    void argumentos();
    void listaDeParametros();
    void listaDeParametrosAux();
    void comandoComposto();
    void comandosOpcionais();
    void listaDeComandos();
    void listaDeComandosAux();
    void comando();
    void parteElse();
    void ativacaoDeProcedimento();
    void listaDeExpressoes();
    void listaDeExpressoesAux();
    void expressao();
    void expressaoND();
    void expressaoSimples();
    void expressaoSimplesAux();
    void termo();
    void termoAux();
    void fator();
    void fatorND();
    void sinal();
    void operadorRelacional();
    void operadorAditivo();
    void operadorMultiplicativo();
    
    bool acabouTokens(string mensagemDeErro);
    bool temErro();
    bool ehFator();
    bool ehOperadorAditivo();

    void abreEscopo();
    void verificaIdentificador(string identificador);
    void fechaEscopo();

};

#endif /* defined(__Analisador_Le_xico__AnalisadorSintatico__) */
