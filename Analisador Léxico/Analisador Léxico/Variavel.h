//
//  Variavel.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 08/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Variavel__
#define __Analisador_Le_xico__Variavel__

#include <iostream>
#include <cstdlib>

typedef enum {
    NAO_MARCADO,
    MARCADOR,
    INTEIRO,
    REAL,
    BOOLEAN,
    NAO_E_VARIAVEL
}Tipo_Variavel;

using namespace std;

#define MARK "$"

class Variavel {
    string identificador;
    Tipo_Variavel tipo;

public:
    Variavel();
    Variavel(string identificador, Tipo_Variavel t);
    
    void setIdentificador(string identificador);
    void setTipo(Tipo_Variavel t);
    string getIdentificador();
    string getTipo();
    
};

#endif /* defined(__Analisador_Le_xico__Variavel__) */
