//
//  Estado.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Estado.h"

Estado::Estado()
{
    this->transicoes = new LinkedList<Transicao>();
}

Estado::Estado(string nome, Tipo t)
{
    this->transicoes = new LinkedList<Transicao>();
    this->nome = nome;
    this->tipo = t;
}

Estado::Estado(string nome, Tipo t, Classificacao c)
{
    this->transicoes = new LinkedList<Transicao>();
    this->nome = nome;
    this->tipo = t;
    if(this->tipo == 2)
        this->classificacao = c;
}


Estado::~Estado()
{
    if (transicoes)
        delete transicoes;
}

void Estado::addTransicao(Transicao *t)
{
    this->transicoes->addLast(t);
}

Estado* Estado::proximoEstado(char simbolo)
{
    Transicao *t;
    for (int i = 0; i < this->transicoes->getSize(); ++i) {
        t = this->transicoes->get(i);
        
        if(t->getSimbolo() == simbolo)
            return t->getProximoEstado();
    }
    
    return NULL;
}

void Estado::listarTransicoes()
{
    string n = this->getNome();
    Transicao *t;
    for (int i = 0; i < this->transicoes->getSize(); ++i) {
        t = this->transicoes->get(i);
        cout << n << ":"<< t->getSimbolo() << " -> " << t->getProximoEstado()->getNome() << "\n";
    }
    
    cout << "\n";
}

void Estado::setNome(string nome)
{
    this->nome = nome;
}

void Estado::setTransicoes(LinkedList<Transicao> *transicoes)
{
    this->transicoes = transicoes;
}

void Estado::setTipo(Tipo t)
{
    this->tipo = t;
}

void Estado::setClassificao(Classificacao c){
    this->classificacao = c;
}

string Estado::getNome()
{
    return this->nome;
}

LinkedList<Transicao>* Estado::getTransicoes()
{
    return transicoes;
}

string Estado::getTipo()
{
    switch (this->tipo) {
        case 0:
            return "Estado Inicial";
            break;
            
        case 1:
            return "Estado Transitório";
            break;
            
        case 2:
            return "Estado Final";
            break;
            
        default:
            break;
    }
}

string Estado::getClassificacao()
{
    if(this->tipo != 2)
        return "";
    
    switch (this->classificacao) {
        case 0:
            return "Atribuição";
            break;
            
        case 1:
            return "Delimitador";
            break;
            
        case 2:
            return "Identificador";
            break;
            
        case 3:
            return "Inteiro";
            break;
            
        case 4:
            return "Real";
            break;
            
        case 5:
            return "Operador Aditivo";
            break;
            
        case 6:
            return "Operador Multiplicativo";
            break;
            
        case 7:
            return "Operador Relacional";
            break;
            
        case 8:
            return "Operador de Diferença";
            break;
            
        case 9:
            return "Palavra Reservada";
            break;
            
        default: return "";
            break;
    }
}
