//
//  Transicao.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Transicao__
#define __Analisador_Le_xico__Transicao__

#include "Estado.h"

//Importante para o compilador por causa do import
class Estado;

class Transicao {
    char simbolo;
    Estado *prox;
    
public:
    Transicao();
    Transicao(char simbolo, Estado *prox);
    
    virtual ~Transicao();

    void setSimbolo(char simbolo);
    void setProximoEstado(Estado *prox);

    char getSimbolo();
    Estado* getProximoEstado();
    
    bool operator==(Transicao transicao);
};

#endif /* defined(__Analisador_Le_xico__Transicao__) */
