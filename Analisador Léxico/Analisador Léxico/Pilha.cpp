//
//  Pilha.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Pilha.h"

Pilha::Pilha():LinkedList<Variavel>()
{

}

bool Pilha::temSimbolo(string simbolo)
{
    Variavel *atual = getFirst();
    
    while (atual != 0) {
        if(atual->getIdentificador().compare(simbolo) == 0)   return true;
        atual = getNext();
    }
    return false;
}

bool Pilha::estaNoEscopo(string simbolo)
{
    Variavel *atual = getFirst();
    
    while (atual != 0) {
        if(atual->getIdentificador().compare(simbolo) == 0)   return true;
        else if(atual->getIdentificador().compare(MARK) == 0) break;
        atual = getNext();
    }

    return false;
}

void Pilha::desempilhaEscopo()
{
    Variavel *atual = getFirst();

    while (atual != 0) {
        if(atual->getIdentificador().compare(MARK) == 0){
            removeFirst();
            break;
        }
        removeFirst();
        atual = getFirst();
    }
//    imprimePilha();
}

void Pilha::setTipoDasVariaveisNaoMarcadas(string nomeTipo)
{
    Variavel *atual = getFirst();
    
    while (atual != 0) {
        if(atual->getIdentificador().compare(MARK) == 0) break;
        
        if(atual->getTipo().compare("não marcado") == 0){
            Tipo_Variavel t;
            
            if(nomeTipo.compare("integer") == 0) t = INTEIRO;
            else if(nomeTipo.compare("real") == 0) t = REAL;
            else if(nomeTipo.compare("boolean") == 0) t = BOOLEAN;
            else t = NAO_E_VARIAVEL;
            
            atual->setTipo(t);
        }
        
        atual = getNext();
    }
}

string Pilha::getTipoDaVariavel(string variavel)
{
    if(variavel.compare("true") == 0 || variavel.compare("false") == 0)
        return "boolean";
    
    Variavel *atual = getFirst();
    
    while (atual != 0) {
        if(atual->getIdentificador().compare(variavel) == 0 &&
           atual->getTipo().compare("não marcado") != 0 &&
            atual->getTipo().compare("mark") != 0 &&
           atual->getTipo().compare("não é variável") != 0) return  atual->getTipo();
        atual = getNext();
    }
    
    return "";
}

void Pilha::imprimePilha()
{
    Variavel *atual = getFirst();
    
    while (atual != 0) {
        cout << atual->getIdentificador() << ": " << atual->getTipo() << endl;
        atual = getNext();
    }
}
