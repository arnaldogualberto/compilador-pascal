/* 
 * File:   Arquivos.cpp
 * Author: ygd
 * 
 * Created on 16 de Janeiro de 2013, 20:48
 */

#include <stdio.h>
#include <iosfwd>
#include <iostream>
#include <fstream>
#include "Arquivos.h"

Arquivos::Arquivos() {
}

Arquivos::Arquivos(const Arquivos& orig) {
}

Arquivos::~Arquivos() {
}

FILE* Arquivos::abrirArquivo(const char* caminho){
    FILE* arq = fopen(caminho, "r");
    return arq;
}

void Arquivos::fecharArquivo(FILE* f){
    fclose(f);
}
