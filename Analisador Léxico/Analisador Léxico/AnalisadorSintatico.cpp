//
//  AnalisadorSintatico.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 14/02/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "AnalisadorSintatico.h"

//Construtores
AnalisadorSintatico::AnalisadorSintatico()
{
    this->erros = new LinkedList<Erro>();
    this->contadorComandoComposto = 0;
    this->pilha = new Pilha();
    this->pilhaExpressao = new PilhaExpressao();
}

AnalisadorSintatico::AnalisadorSintatico(LinkedList<Tabela> *saidaLexico)
{
    this->saidaLexico = saidaLexico;
    this->erros = new LinkedList<Erro>();
    this->pilha = new Pilha();
    this->pilhaExpressao = new PilhaExpressao();
    this->contadorComandoComposto = 0;
}

//Métodos Públicos
void AnalisadorSintatico::AnalisadorSintatico::imprimeErros()
{
    for (int i = 0; i < erros->getSize(); ++i)
        this->erros->get(i)->mostraErro();
}

int AnalisadorSintatico::getNumeroDeErros()
{
    return this->erros->getSize();
}

//Analisa o primeiro bloco do programa
void AnalisadorSintatico::AnalisadorSintatico::initAnaliseSintatica()
{
    if(!this->saidaLexico){
        std::cout << "Erro: Saída do Léxico não recebida!" << endl;
        return;
    }
    
    if(this->saidaLexico->isEmpty()){
        std::cout << "Erro: Lista de Tokens vazia!" << endl;
        return;
    }

    if(proximoSimbolo()->getToken().compare("program") == 0){
        this->saidaLexico->removeFirst();        
        if(acabouTokens("Identificador")) return;
        
        if(proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
            abreEscopo();
            this->pilha->addFirst(new Variavel(proximoSimbolo()->getToken(), NAO_E_VARIAVEL));
            this->saidaLexico->removeFirst();
            if(acabouTokens("';'")) return;
            
            if(proximoSimbolo()->getToken().compare(";") == 0){
                this->saidaLexico->removeFirst();
                if(acabouTokens("declaração de variável") || temErro()) return;
                
                declaracoesDeVariaveis();
                if(acabouTokens("declaração de subprogramas ou comando composto") || temErro()) return;
                
                declaracoesDeSubprogramas();
                if(acabouTokens("comando composto") || temErro()) return;
                
                comandoComposto();
                if(acabouTokens(".") || temErro()) return;
                
                if(proximoSimbolo()->getToken().compare(".") == 0)
                    this->saidaLexico->removeFirst();
                else
                    this->erros->addLast(new Erro("'.'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));                    
                
            }else{//;
                this->erros->addLast(new Erro("';'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
            }
        }else{//Identificador
            this->erros->addLast(new Erro("identificador", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }else{//program
        this->erros->addLast(new Erro("'program'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

//Métodos Privados
Tabela* AnalisadorSintatico::proximoSimbolo()
{
    Tabela *prox = this->saidaLexico->getFirst();
    return prox;
}

//Começa a declaração de variáveis (var)
void AnalisadorSintatico::declaracoesDeVariaveis()
{
    //Começa a declaração de variáveis
    if(proximoSimbolo()->getToken().compare("var") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("identificador") || temErro()) return;
        
        listaDeDeclaracoesDeVariaveis();
        if(temErro()) return;
    }
}

void AnalisadorSintatico::listaDeDeclaracoesDeVariaveis()
{
    listaDeIdentificadores();
    if(acabouTokens("identificador") || temErro()) return;
    
    if(proximoSimbolo()->getToken().compare(":")==0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("tipo") || temErro()) return;
        
        tipo();
        if(acabouTokens("';'") ||temErro()) return;
        
        if(proximoSimbolo()->getToken().compare(";")==0){
            this->saidaLexico->removeFirst();
            if(acabouTokens("variáveis ou declarações de subprogramas")) return;
            
            listaDeDeclaracoesDeVariaveisAux();
        }else{
            this->erros->addLast(new Erro("';'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));            
        }
        
    }else{
        this->erros->addLast(new Erro("':'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::listaDeDeclaracoesDeVariaveisAux()
{
    if(proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
        listaDeIdentificadores();
        if(acabouTokens(":") || temErro()) return;
        
        if(proximoSimbolo()->getToken().compare(":")==0){
            this->saidaLexico->removeFirst();
            if(acabouTokens("tipo") || temErro()) return;
            
            tipo();
            if(acabouTokens("';'") || temErro()) return;
            
            if(proximoSimbolo()->getToken().compare(";")==0){
                this->saidaLexico->removeFirst();
                if(acabouTokens("variáveis ou declarações de subprogramas") || temErro()) return;
                
                listaDeDeclaracoesDeVariaveisAux();
            }else{
                this->erros->addLast(new Erro("';'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));                
            }
            
        }else{
            this->erros->addLast(new Erro("':'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

//Verifica identificadores separados por vírgula
void AnalisadorSintatico::listaDeIdentificadores()
{
    if(proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
        verificaIdentificador(proximoSimbolo()->getToken());
        this->saidaLexico->removeFirst();
        if(acabouTokens("identificador") || temErro()) return;
        
        listaDeIdentificadoresAux();
    }else{
        this->erros->addLast(new Erro("identificador", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));        
    }
}

void AnalisadorSintatico::listaDeIdentificadoresAux()
{
    if(proximoSimbolo()->getToken().compare(",") == 0){
        this->saidaLexico->removeFirst();        
        if(acabouTokens("identificador") || temErro()) return;
        
        if(proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
            verificaIdentificador(proximoSimbolo()->getToken());
            this->saidaLexico->removeFirst();
            if(acabouTokens("identificador") || temErro()) return;
            
            listaDeIdentificadoresAux();
        }else{
            this->erros->addLast(new Erro("identificador", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

void AnalisadorSintatico::tipo()
{
    if((proximoSimbolo()->getToken().compare("integer") == 0)||
       (proximoSimbolo()->getToken().compare("real") == 0)||
       (proximoSimbolo()->getToken().compare("boolean") == 0)){
        this->pilha->setTipoDasVariaveisNaoMarcadas(proximoSimbolo()->getToken());
        this->saidaLexico->removeFirst();
    }else{
        this->erros->addLast(new Erro("tipo", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::declaracoesDeSubprogramas()
{
    if(proximoSimbolo()->getToken().compare("procedure") == 0){
        declaracoesDeSubprogramasAux();
    }
}

void AnalisadorSintatico::declaracoesDeSubprogramasAux()
{
    if(proximoSimbolo()->getToken().compare("procedure") == 0){
        declaracaoDeSubprograma();
        if(acabouTokens(";") || temErro()) return;
    
        if(proximoSimbolo()->getToken().compare(";") == 0){
            this->saidaLexico->removeFirst();
            if(acabouTokens("declaracões de subprogramas") || temErro()) return;
        
            declaracoesDeSubprogramasAux();
        }else{
            this->erros->addLast(new Erro("';'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

void AnalisadorSintatico::declaracaoDeSubprograma()
{
    if(proximoSimbolo()->getToken().compare("procedure") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("identificador") || temErro()) return;
        
        if(proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
            verificaIdentificador(proximoSimbolo()->getToken());
            abreEscopo();
            this->saidaLexico->removeFirst();
            if(acabouTokens("argumentos ou ';'") || temErro()) return;
            
            argumentos();
            
            if(acabouTokens("';'") || temErro()) return;
            
            if(proximoSimbolo()->getToken().compare(";") == 0){
                this->saidaLexico->removeFirst();
                if(acabouTokens("declaração do variáveis") || temErro()) return;
                
                declaracoesDeVariaveis();
                if(acabouTokens("declaração do subprograma") || temErro()) return;
                
                declaracoesDeSubprogramas();
                if(acabouTokens("declaração do subprograma") || temErro()) return;
                
                comandoComposto();
            }else{
                this->erros->addLast(new Erro("';'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
            }
        }
    }else{
        this->erros->addLast(new Erro("'procedure'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));        
    }
}

void AnalisadorSintatico::argumentos()
{
    if(proximoSimbolo()->getToken().compare("(") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("identificador") || temErro()) return;
        
        listaDeParametros();
        if(acabouTokens(")") || temErro()) return;
        
        if(proximoSimbolo()->getToken().compare(")") == 0){
            this->saidaLexico->removeFirst();            
        }else{
            this->erros->addLast(new Erro("')'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

void AnalisadorSintatico::listaDeParametros()
{
    listaDeIdentificadores();
    if(acabouTokens("identificador") || temErro()) return;
    
    if(proximoSimbolo()->getToken().compare(":") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("tipo") || temErro()) return;
        
        tipo();
        if(acabouTokens(")") || temErro()) return;
        
        listaDeParametrosAux();
    }else{
        this->erros->addLast(new Erro("':'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::listaDeParametrosAux()
{
    if(proximoSimbolo()->getToken().compare(";") == 0){
        this->saidaLexico->removeFirst();
        
        listaDeIdentificadores();
        if(acabouTokens("identificador") || temErro()) return;
    
        if(proximoSimbolo()->getToken().compare(":") == 0){
            this->saidaLexico->removeFirst();
            if(acabouTokens("tipo") || temErro()) return;
        
            tipo();
            if(acabouTokens(")") || temErro()) return;
        
            listaDeParametrosAux();
        }else{
            this->erros->addLast(new Erro("':'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

void AnalisadorSintatico::comandoComposto()
{
    if(proximoSimbolo()->getToken().compare("begin") == 0){
        this->contadorComandoComposto++;
        this->saidaLexico->removeFirst();
        if(acabouTokens("comando") || temErro()) return;
        
        comandosOpcionais();
        if(acabouTokens("comando") || temErro()) return;

        if(proximoSimbolo()->getToken().compare("end") == 0){
            if(!this->pilhaExpressao->atualizaTopo())
                this->erros->addLast(new Erro("tipos incompativeis", this->saidaLexico->getFirst()->getLinha(), ""));
            this->pilhaExpressao = new PilhaExpressao();
            this->fechaEscopo();
            this->saidaLexico->removeFirst();
        }else{
            this->erros->addLast(new Erro("'end'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }else{
        this->erros->addLast(new Erro("'begin'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::comandosOpcionais()
{
    if ((proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0) ||
        (proximoSimbolo()->getToken().compare("begin") == 0) ||
        (proximoSimbolo()->getToken().compare("while") == 0) ||
        (proximoSimbolo()->getToken().compare("if") == 0)  ) {
        listaDeComandos();
    }
}

void AnalisadorSintatico::listaDeComandos()
{
    comando();
    if(acabouTokens("comando") || temErro()) return;
    
    listaDeComandosAux();
}

void AnalisadorSintatico::listaDeComandosAux()
{
    if(proximoSimbolo()->getToken().compare(";") == 0){
        if(!this->pilhaExpressao->atualizaTopo())
            this->erros->addLast(new Erro("tipos incompativeis", this->saidaLexico->getFirst()->getLinha(), ""));
        this->pilhaExpressao = new PilhaExpressao();
        this->saidaLexico->removeFirst();
        if(acabouTokens("comando") || temErro()) return;
        
        comando();
        if(acabouTokens("comando") || temErro()) return;
        
        listaDeComandosAux();
    }   
}

void AnalisadorSintatico::comando()
{
    if(proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
        verificaIdentificador(this->proximoSimbolo()->getToken());
        
        string tipo_da_variavel = this->pilha->getTipoDaVariavel(proximoSimbolo()->getToken());     
        if(tipo_da_variavel.compare("") != 0)
            this->pilhaExpressao->addFirst(new Expressao(tipo_da_variavel));
        
        this->saidaLexico->removeFirst();
        if(acabouTokens("':=' ou ativação de procedimento") || temErro()) return;
        
        if(proximoSimbolo()->getToken().compare(":=") == 0){
            this->saidaLexico->removeFirst();
            if(acabouTokens("expressão") || temErro()) return;
            
            expressao();
        }
        else if(proximoSimbolo()->getToken().compare("(") == 0){
            ativacaoDeProcedimento();
        }
    }else if(proximoSimbolo()->getToken().compare("begin") == 0){
        comandoComposto();
    }else if(proximoSimbolo()->getToken().compare("if") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão") || temErro()) return;
        
        expressao();
        if(acabouTokens("then") || temErro()) return;
        
        if(proximoSimbolo()->getToken().compare("then") == 0){
            this->saidaLexico->removeFirst();
            if(acabouTokens("comando") || temErro()) return;
            
            comando();
            if(acabouTokens("else") || temErro()) return;
            
            parteElse();
        }else{
            this->erros->addLast(new Erro("then", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }else if(proximoSimbolo()->getToken().compare("while") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão") || temErro()) return;
        
        expressao();
        if(acabouTokens("do") || temErro()) return;
        
        if(proximoSimbolo()->getToken().compare("do") == 0){
            //Apagar depois
            this->pilhaExpressao = new PilhaExpressao();
            this->saidaLexico->removeFirst();
            if(acabouTokens("comando") || temErro()) return;
            
            comando();
        }
    }else{
        this->erros->addLast(new Erro("comando", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));        
    }
}

void AnalisadorSintatico::parteElse()
{
    if(proximoSimbolo()->getToken().compare("else") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("comando") || temErro()) return;
        
        comando();
    }
}

void AnalisadorSintatico::ativacaoDeProcedimento()
{
    if(proximoSimbolo()->getToken().compare("(") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão") || temErro()) return;
        
        listaDeExpressoes();
        if(acabouTokens("')'") || temErro()) return;
        
        if (proximoSimbolo()->getToken().compare(")") == 0) {
            this->saidaLexico->removeFirst();
        }else{
            this->erros->addLast(new Erro("'begin'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

void AnalisadorSintatico::listaDeExpressoes()
{
    expressao();
    if(acabouTokens("','") || temErro()) return;
    
    listaDeExpressoesAux();
}

void AnalisadorSintatico::listaDeExpressoesAux()
{
    if(proximoSimbolo()->getToken().compare(",") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão") || temErro()) return;
        
        expressao();
        if(acabouTokens(",") || temErro()) return;
        
        listaDeExpressoesAux();
    }
}

void AnalisadorSintatico::expressao()
{
    expressaoSimples();
    if(acabouTokens("expressão") || temErro()) return;
    
    expressaoND();
}

void AnalisadorSintatico::expressaoND()
{
    if(proximoSimbolo()->getEstado()->getClassificacao().compare("Operador Relacional") == 0){
        operadorRelacional();
        if(acabouTokens("expressão") || temErro()) return;
        
        expressaoSimples();
    }
}

void AnalisadorSintatico::expressaoSimples()
{
    //Verifica se é expressão simples
    if ( (proximoSimbolo()->getEstado()->getClassificacao().compare("Operador aditivo") == 0) || ehFator()) {
        if (proximoSimbolo()->getEstado()->getClassificacao().compare("Operador aditivo") == 0){
            sinal();
            if(acabouTokens("termo") || temErro()) return;
            
            termo();
            if(acabouTokens("expressão") || temErro()) return;
            
            expressaoSimplesAux();
        }else{
            termo();
            if(acabouTokens("expressão") || temErro()) return;
            
            expressaoSimplesAux();
        }
    }else {
        this->erros->addLast(new Erro("expressão simples", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::expressaoSimplesAux()
{
    if(ehOperadorAditivo()){
        operadorAditivo();
        if(acabouTokens("termo") || temErro()) return;
        
        termo();
        if(acabouTokens("termo") || temErro()) return;
        
        expressaoSimplesAux();
    }
    
    if(proximoSimbolo()->getEstado()->getClassificacao().compare("Operador Aditivo") == 0){
        sinal();
        if(acabouTokens("termo") || temErro()) return;
        
        termo();
        if(acabouTokens("expressão") || temErro()) return;
        
        expressaoSimplesAux();
    }
}

void AnalisadorSintatico::termo()
{
    if(ehFator()){
        fator();
        if(acabouTokens("termo") || temErro()) return;
        
        termoAux();
    }
}

void AnalisadorSintatico::termoAux()
{
    if(proximoSimbolo()->getEstado()->getClassificacao().compare("Operador Multiplicativo") == 0){
        operadorMultiplicativo();
        if(acabouTokens("fator") || temErro()) return;
        
        fator();
        if(acabouTokens("termo") || temErro()) return;
        
        termoAux();
    }
}

void AnalisadorSintatico::fator()
{
    if( (proximoSimbolo()->getEstado()->getClassificacao().compare("Inteiro") == 0) ||
        (proximoSimbolo()->getEstado()->getClassificacao().compare("Real") == 0) ||
        (proximoSimbolo()->getToken().compare("true") == 0) ||
        (proximoSimbolo()->getToken().compare("false") == 0)){
        
        if( proximoSimbolo()->getEstado()->getClassificacao().compare("Inteiro") == 0)
            this->pilhaExpressao->addFirst(new Expressao("inteiro"));
        else if( proximoSimbolo()->getEstado()->getClassificacao().compare("Real") == 0)
            this->pilhaExpressao->addFirst(new Expressao("real"));
        else
            this->pilhaExpressao->addFirst(new Expressao("boolean"));
        
        this->saidaLexico->removeFirst();
    }else if(proximoSimbolo()->getToken().compare("not") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("fator") || temErro()) return;
        
        fator();
    }else if (proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0){
        verificaIdentificador(proximoSimbolo()->getToken());

        string tipo_da_variavel = this->pilha->getTipoDaVariavel(proximoSimbolo()->getToken());
        if(tipo_da_variavel.compare("") != 0)
            this->pilhaExpressao->addFirst(new Expressao(tipo_da_variavel));
        
        this->saidaLexico->removeFirst();
        if(acabouTokens("fator") || temErro()) return;
        
        fatorND();
    }else if( proximoSimbolo()->getToken().compare("(") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão") || temErro()) return;
        
        expressao();
        if(acabouTokens("')'") || temErro()) return;        
        
        if(proximoSimbolo()->getToken().compare(")") == 0){
            if(!this->pilhaExpressao->atualizaTopo())
                this->erros->addLast(new Erro("tipos incompativeis", this->saidaLexico->getFirst()->getLinha(), ""));
            this->saidaLexico->removeFirst();
        }else{
            this->erros->addLast(new Erro("')'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }else{
        this->erros->addLast(new Erro("fator", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));        
    }
}

void AnalisadorSintatico::fatorND()
{
    if( proximoSimbolo()->getToken().compare("(") == 0){
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão") || temErro()) return;
        
        listaDeExpressoes();
        if(acabouTokens("')'") || temErro()) return;
        
        if(proximoSimbolo()->getToken().compare(")") == 0){
            this->saidaLexico->removeFirst();
        }else{
            this->erros->addLast(new Erro("')'", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
        }
    }
}

void AnalisadorSintatico::sinal()
{
    if( (proximoSimbolo()->getToken().compare("+") == 0) ||
        (proximoSimbolo()->getToken().compare("-") == 0)){
        this->saidaLexico->removeFirst();
        if(acabouTokens("identificador") || temErro()) return;
    }else{
        this->erros->addLast(new Erro("Sinal", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::operadorRelacional()
{
    if (proximoSimbolo()->getEstado()->getClassificacao().compare("Operador Relacional") == 0) {
        this->saidaLexico->removeFirst();
        if(acabouTokens("expressão simples") || temErro()) return;
    }else{
        this->erros->addLast(new Erro("Operador Relacional", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

void AnalisadorSintatico::operadorAditivo()
{
    if( (proximoSimbolo()->getToken().compare("+") == 0) ||
        (proximoSimbolo()->getToken().compare("-") == 0) ||
        (proximoSimbolo()->getToken().compare("or") == 0) ){
        this->saidaLexico->removeFirst();
        if(acabouTokens("identificador") || temErro()) return;
    }else{
        this->erros->addLast(new Erro("Operador aditivo", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));        
    }
}

void AnalisadorSintatico::operadorMultiplicativo()
{
    if( (proximoSimbolo()->getEstado()->getClassificacao().compare("Operador Multiplicativo") == 0) ||
        (proximoSimbolo()->getToken().compare("and") == 0)){
        this->saidaLexico->removeFirst();
    }else{
        this->erros->addLast(new Erro("Operador multiplicativo", this->saidaLexico->getFirst()->getLinha(), this->saidaLexico->getFirst()->getToken()));
    }
}

bool AnalisadorSintatico::acabouTokens(string mensagemDeErro)
{
    if(this->saidaLexico->isEmpty()){
        this->erros->addLast(new Erro(mensagemDeErro));
        return true;
    }else
        return false;
}

bool AnalisadorSintatico::temErro()
{
    return this->erros->isEmpty() ? false : true;
}

bool AnalisadorSintatico::ehFator()
{
    if((proximoSimbolo()->getEstado()->getClassificacao().compare("Identificador") == 0) ||
       (proximoSimbolo()->getEstado()->getClassificacao().compare("Inteiro") == 0) ||
       (proximoSimbolo()->getEstado()->getClassificacao().compare("Real") == 0) ||
       (proximoSimbolo()->getToken().compare("true") == 0) ||
       (proximoSimbolo()->getToken().compare("false") == 0) ||
       (proximoSimbolo()->getToken().compare("not") == 0) ||
       (proximoSimbolo()->getToken().compare("(") == 0))
        return true;
    else
        return false;
}

bool AnalisadorSintatico::ehOperadorAditivo()
{
    if((proximoSimbolo()->getEstado()->getClassificacao().compare("Operador Aditivo") == 0) ||
       (proximoSimbolo()->getToken().compare("or") == 0))
        return true;
    else
        return false;
}

void AnalisadorSintatico::abreEscopo()
{
    this->pilha->addFirst(new Variavel(MARK, MARCADOR));
}

void AnalisadorSintatico::verificaIdentificador(string identificador)
{
    if(this->contadorComandoComposto == 0){
        if(!this->pilha->estaNoEscopo(identificador))
            this->pilha->addFirst(new Variavel(identificador, NAO_MARCADO));
        else
            this->erros->addLast(new Erro("Duplicado Identificador: "+identificador));
    }else{
        //Está dentro do comando composto [begin ... end]
        if(!this->pilha->temSimbolo(identificador))
            this->erros->addLast(new Erro("Identificador: "+identificador +" não"));
    }
}

void AnalisadorSintatico::fechaEscopo()
{    
    this->contadorComandoComposto--;
    if(this->contadorComandoComposto == 0){
        this->pilha->desempilhaEscopo();
    }
}
