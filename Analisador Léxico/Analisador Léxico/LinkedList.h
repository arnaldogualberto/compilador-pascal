#ifndef LINKEDLIST_H
#define	LINKEDLIST_H

#include "NodeList.h"
#include <iostream>
using namespace std;

template <class Element>

class LinkedList {
    
protected:
    NodeList<Element>* first;
    NodeList<Element>* last;
    NodeList<Element>* iterator;
    unsigned size;
    
public:
    LinkedList() {
        size = 0;
        first = 0;
        last = 0;
        iterator = 0;
    }
    
    virtual ~LinkedList() {
        while(size != 0) {
            removeLast();
        }
    }
    
    unsigned getSize() {
        return size;
    }
    
    void addFirst(Element data) {
        NodeList<Element>* aux = new NodeList<Element>(data, 0, first);
        
        if(first) {
            first->setPrevious(aux);
            first = aux;
        } else {
            first = aux;
            last = aux;
        }
        
        size++;
    }
    
    void addFirst(Element* data) {
        NodeList<Element>* aux = new NodeList<Element>(data, 0, first);
        
        if(first) {
            first->setPrevious(aux);
            first = aux;
        } else {
            first = aux;
            last = aux;
        }
        
        size++;
    }
    
    void addLast(Element data) {
        NodeList<Element>* aux = new NodeList<Element>(data, last, 0);
        
        if(last) {
            last->setNext(aux);
            last = aux;
        } else {
            first = aux;
            last = aux;
        }
        
        size++;
    }
    
    void addLast(Element* data) {
        NodeList<Element>* aux = new NodeList<Element>(data, last, 0);
        
        if(last) {
            last->setNext(aux);
            last = aux;
        } else {
            first = aux;
            last = aux;
        }
        
        size++;
    }
    
    void add(Element data, int index) {
        if((index == 0) || (size == 0)){
            addFirst(data);
            return;
        }
        
        if(index == (size - 1)) {
            addLast(data);
            return;
        }
        
        if(index >= size) {
            return;
        }
        
        NodeList<Element>* temp = new NodeList<Element>(data);
        
        int i;
        NodeList<Element> *aux;
        for(aux = first, i = 0 ; i != index ; aux = (aux->getNext()), i++) {
            
        }
        
        temp->setPrevious(aux->getPrevious());
        temp->setNext(aux);
        
        if(temp->getPrevious()) {
            temp->getPrevious()->setNext(temp);
        }
        
        if(temp->getNext()) {
            temp->getNext()->setPrevious(temp);
        }
        
        size++;
    }
    
    void add(Element* data, int index) {
        if((index == 0) || (size == 0)){
            addFirst(data);
            return;
        }
        
        if(index == (size - 1)) {
            addLast(data);
            return;
        }
        
        if(index >= size) {
            return;
        }
        
        NodeList<Element>* temp = new NodeList<Element>(data);
        
        int i;
        NodeList<Element> *aux;
        for(aux = first, i = 0 ; i != index ; aux = (aux->getNext()), i++) {
            
        }
        
        temp->setPrevious(aux->getPrevious());
        temp->setNext(aux);
        
        if(temp->getPrevious()) {
            temp->getPrevious()->setNext(temp);
        }
        
        if(temp->getNext()) {
            temp->getNext()->setPrevious(temp);
        }
        
        size++;
    }
    
    void add(Element data) {
        if(size == 0) {
            addFirst(data);
            return;
        }
            
        if(data < *(first->getData())) {
            addFirst(data);
            return;
        }
        
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = (aux->getNext())) {
            if(data < *(aux->getData())) {
                NodeList<Element>* temp = new NodeList<Element>(data);
                
                temp->setPrevious(aux->getPrevious());
                temp->setNext(aux);

                if(temp->getPrevious()) {
                    temp->getPrevious()->setNext(temp);
                }
                
                if(temp->getNext()) {
                    temp->getNext()->setPrevious(temp);
                }
        
                size++;
                
                return;
            }
        }
        
        addLast(data);
    }
    
    void add(Element* data) {
        if(size == 0) {
            addFirst(data);
            return;
        }
            
        if(*(data) < *(first->getData())) {
            addFirst(data);
            return;
        }
        
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = (aux->getNext())) {
            if((*data) < *(aux->getData())) {
                NodeList<Element>* temp = new NodeList<Element>(data);
                
                temp->setPrevious(aux->getPrevious());
                temp->setNext(aux);

                if(temp->getPrevious()) {
                    temp->getPrevious()->setNext(temp);
                }
                
                if(temp->getNext()) {
                    temp->getNext()->setPrevious(temp);
                }
        
                size++;
                
                return;
            }
        }
        
        addLast(data);
    }
    
    void removeLast() {
        if(size == 0) {
            return;
        }
        
        NodeList<Element> *temp = last;
        
        if(last->getPrevious()) {
            last->getPrevious()->setNext(0);
        }
        last = last->getPrevious();
        
        temp->~NodeList();
        
        size--;
    }
    
    void removeFirst() {
        if(size == 0) {
            return;
        }
        
        NodeList<Element> *temp = first;
        
        if(first ->getNext()) {
            first->getNext()->setPrevious(0);
        }
        first = first->getNext();
        
        temp->~NodeList();
        
        size--;
    }
    
    void remove(int index) {
        if(index == 0) {
            removeFirst();
            return;
        }
        
        if(index == (size - 1)) {
            removeLast();
            return;
        }
        
        if(size == 0) {
            return;
        }
        
        int i;
        NodeList<Element> *aux;
        for(aux = first, i = 0 ; i != index ; aux = aux->getNext(), i++) {
            
        }
        
        aux->getPrevious()->setNext(aux->getNext());
        aux->getNext()->setPrevious(aux->getPrevious());
        
        aux->~NodeList();
        
        size--;
    }
    
    void clear() {
        while(size != 0) {
            removeLast();
        }
    }
    
    bool contains(Element data) {
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = aux->getNext()) {
            if(*(aux->getData()) == data) {
                return true;
            }
        }
        return false;
    }
    
    bool contains(Element* data) {
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = aux->getNext()) {
            if(*(aux->getData()) == *data) {
                return true;
            }
        }
        return false;
    }
    
    Element* get(Element data) {
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = aux->getNext()) {
            if(*(aux->getData()) == data) {
                iterator = aux;
                return aux->getData();
            }
        }
        
        return 0;
    }
    
    Element* get(Element* data) {
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = aux->getNext()) {
            if(*(aux->getData()) == *data) {
                iterator = aux;
                return aux->getData();
            }
        }
        return 0;
    }
    
    Element* get(unsigned index) {
        NodeList<Element> *aux;
        unsigned i;
        for(i = 0, aux = first ; aux ; aux = aux->getNext(), i++) {
            if(i == index) {
                iterator = aux;
                return aux->getData();
            }
        }
        return 0;
    }
    
    Element* getLast() {
        if(last) {
            iterator = last;
            return last->getData();
        } else {
            iterator = 0;
            return 0;
        }
    }
    
    Element* getFirst() {
        if(first) {
            iterator = first;
            return first->getData();
        } else {
            iterator = 0;
            return 0;
        }
    }
    
    Element* getNext() {
        if(!iterator) {
            return 0;
        }
        
        iterator = iterator->getNext();
        
        if(!iterator) {
            return 0;
        }
        
        return iterator->getData();
    }
    
    Element* getPrevious() {
        if(!iterator) {
            return 0;
        }
        
        iterator = iterator->getPrevious();
        
        if(!iterator) {
            return 0;
        }
        return iterator->getData();
    }
    
    bool isEmpty(){
        return (size == 0 ? true : false);
    }
    
    void print() {
        NodeList<Element> *aux;
        for(aux = first ; aux ; aux = aux->getNext()) {
            cout << *(aux->getData()) << endl;
        }
    }
    
    Element* operator[](unsigned index) {
        return get(index);
    }

};

#endif