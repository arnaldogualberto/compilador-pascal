//
//  Tabela.cpp
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 16/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#include "Tabela.h"

Tabela::Tabela()
{
    
}

Tabela::Tabela(string token, string classificacao, int linha)
{
    this->token = token;
    this->classificacao = classificacao;
    this->linha = linha;
}

Tabela::Tabela(string token, Estado *estado, int linha)
{
    this->token = token;
    this->estado = estado;
    this->linha = linha;
}

void Tabela::mostrarTabela(LinkedList<Tabela> *tab)
{    
    int maiorToken = 0;
    int maiorLinha = 0;
    
    //Calcula o tamanho das maiores strings para token e linha.
    for(int i=0; i < tab->getSize(); i++){
        int maiorTokenAtual = (int)tab->get(i)->token.length();
        if(maiorTokenAtual > maiorToken)
            maiorToken = maiorTokenAtual;
        
        int maiorLinhaAtual = (int)(to_string((int)tab->get(i)->linha).length());
        if(maiorLinhaAtual > maiorLinha)
            maiorLinha = maiorLinhaAtual;
    }
    
    for(int i=0; i < tab->getSize(); i++){
        //Imprime o token
        cout << "| " << tab->get(i)->token;
        int tamanhoTokenAtual=(int)tab->get(i)->token.length();
        for (int j=-2; j < (maiorToken - tamanhoTokenAtual) ; j++)//-2 é usado para adicionar +2 espaços em branco
            cout << " ";
        cout << "| ";
        
        //Imprime a classificação
//        string classificacao =  modificaIdentificador(tab->get(i)->token, tab->get(i)->estado->getClassificao());
        modificaIdentificador(tab->get(i));
        string classificacao = tab->get(i)->estado->getClassificacao();
        cout << classificacao;
        int tamanhoClassificacaoAtual= tamanhoStringClassificacao(classificacao);
        for (int j=-2; j < (MAIOR_CLASSIFICADOR-tamanhoClassificacaoAtual) ; j++)
            cout << " ";
        cout << "| ";
        
        //Imprime a linha
        int tamanhoLinhaAtual=(int)(to_string((int)tab->get(i)->linha).length());
        for (int j=-2; j < (maiorLinha-tamanhoLinhaAtual) ; j++)
            cout << " ";
        cout << (int)tab->get(i)->linha << " |";
        
        cout << "\n";
        for(int i = -13; i < (maiorToken+maiorLinha+MAIOR_CLASSIFICADOR); ++i)
            cout << "-";
        cout << "\n";
    }

}
 
//Calcula o tamanho das Strings de Classificação
int Tabela::tamanhoStringClassificacao(string classificacao)
{
    if(classificacao == "Atribuição")
        return 10;
    else if(classificacao == "Operador de Diferença")
        return 21;
    else
        return ((int)classificacao.length());
}

void Tabela::modificaIdentificador(Tabela *t){
    string palavrasReservadas[15] = {"and", "begin", "boolean", "else", "end", "if", "integer", "not", "or", "procedure", "program", "real", "then", "var", "while"};
    
    for (int i = 0; i < 16; i++)
        if (palavrasReservadas[i].compare(t->token) == 0){
            t->estado = (new Grafo())->getEstado22();
            return;
        }
}


string Tabela::getToken()
{
    return this->token;
}

string Tabela::getClassificacao()
{
    return this->classificacao;
}

Estado* Tabela::getEstado()
{
    return this->estado;
}

int Tabela::getLinha()
{
    return this->linha;
}
