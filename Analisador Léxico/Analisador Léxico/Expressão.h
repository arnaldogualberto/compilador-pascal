//
//  Expressão.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 09/03/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Expressa_o__
#define __Analisador_Le_xico__Expressa_o__

#include <iostream>
#include <cstdlib>

using namespace std;

class Expressao {
    string tipo_variavel;
    
public:
    Expressao();
    Expressao(string tipo_variavel);
    
    void setTipoVariavel(string tipo_variavel);
    string getTipoVariavel();
};

#endif /* defined(__Analisador_Le_xico__Expressa_o__) */
