//
//  Grafo.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 16/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Grafo__
#define __Analisador_Le_xico__Grafo__

#include "Estado.h"
#include "LinkedList.h"

class Grafo {
public:
    Estado *e1;
    Estado *e2;
    Estado *e3;
    Estado *e4;
    Estado *e5;
    Estado *e6;
    Estado *e7;
    Estado *e8;
    Estado *e9;
    Estado *e10;
    Estado *e11;
    Estado *e12;
    Estado *e13;
    Estado *e14;
    Estado *e15;
    Estado *e16;
    Estado *e17;
    Estado *e18;
    Estado *e19;
    Estado *e20;
    Estado *e21;
    Estado *e22;
    
public:
    Grafo();
    
    virtual ~Grafo();
    
    LinkedList<Estado>* estados();
    
    void iniciaEstado1();
    void iniciaEstado2();
    void iniciaEstado3();
    void iniciaEstado4();
    void iniciaEstado5();
    void iniciaEstado6();
    void iniciaEstado7();
    void iniciaEstado8();
    void iniciaEstado9();
    void iniciaEstado10();
    void iniciaEstado11();
    void iniciaEstado12();
    void iniciaEstado13();
    void iniciaEstado14();
    void iniciaEstado15();
    void iniciaEstado16();
    void iniciaEstado17();
    void iniciaEstado18();
    void iniciaEstado19();
    void iniciaEstado20();
    void iniciaEstado21();
    void iniciaEstado22();
    
    Estado* getEstado22();
};

#endif /* defined(__Analisador_Le_xico__Grafo__) */
