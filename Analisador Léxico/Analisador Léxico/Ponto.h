//
//  Ponto.h
//  Analisador Léxico
//
//  Created by Arnaldo Gualberto on 07/01/13.
//  Copyright (c) 2013 Arnaldo Gualberto. All rights reserved.
//

#ifndef __Analisador_Le_xico__Ponto__
#define __Analisador_Le_xico__Ponto__

class Ponto {
    int *x;
    int *y;
    
public:
    //Construtores
    Ponto(int *x, int *y);
    Ponto();
    
    //Destrutor
    virtual ~Ponto();
    
    //Acessors
    void setX(int *x);
    void setY(int *y);
    int* getX();
    int* getY();
};

#endif /* defined(__Analisador_Le_xico__Ponto__) */
