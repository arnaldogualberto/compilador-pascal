#ifndef NODELIST_H
#define	NODELIST_H

#include <iostream>

using namespace std;

template <class Element>

class NodeList {

private:
    Element* data;
    NodeList<Element> *previous;
    NodeList<Element> *next;
    
public:
    NodeList() {
        data = 0;
        previous = 0;
        next = 0;
    }
    
    NodeList(Element data) {
        this->data = new Element();
        *(this->data) = data;
        
        previous = 0;
        next = 0;
    }
    
    NodeList(Element* data) {
        this->data = data;
        previous = 0;
        next = 0;
    }
    
    NodeList(Element data, NodeList<Element>* previous, NodeList<Element>* next) {
        this->data = new Element();
        *(this->data) = data;
        this->previous = previous;
        this->next = next;
    }
    
    NodeList(Element* data, NodeList<Element>* previous, NodeList<Element>* next) {
        this->data = data;
        this->previous = previous;
        this->next = next;
    }
    
    virtual ~NodeList() {
        
    }
    
    Element* getData() {
        return this->data;
    }
    
    void setData(Element data) {
        *(this->data) = data;
    }
    
    void setData(Element* data) {
        this->data = data;
    }
    
    NodeList<Element>* getPrevious() {
        return previous;
    }
    
    void setPrevious(NodeList<Element>* previous) {
        this->previous = previous;
    }
    
    NodeList<Element>* getNext() {
        return next;
    }
    
    void setNext(NodeList<Element>* next) {
        this->next = next;
    }
    
    bool operator>(NodeList<Element>* node) {
        return (*(this->data)) > (*(node->getData()));
    }
    
    bool operator<(NodeList<Element>* node) {
        return (*(this->data)) < (*(node->getData()));
    }
    
    bool operator>=(NodeList<Element>* node) {
        return (*(this->data)) >= (*(node->getData()));
    }
    
    bool operator<=(NodeList<Element>* node) {
        return (*(this->data)) <= (*(node->getData()));
    }
    
    bool operator==(NodeList<Element>* node) {
        return (*(this->data)) == (*(node->getData()));
    }
    
};

#endif